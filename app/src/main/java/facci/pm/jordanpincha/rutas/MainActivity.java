package facci.pm.jordanpincha.rutas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    TextView lblnumero, lblavion, lbldate, lblname, lblorigendestino;
    EditText txtnumero, txtavion, txtdate, txtname, txtorigendestino;
    Button btningresar, btnconsultar, btnmodificar, btneliminar, btngeneral;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lblnumero = (TextView) findViewById(R.id.lblnumero);
        lblavion = (TextView) findViewById(R.id.lblavion);
        lbldate = (TextView) findViewById(R.id.lbldate);
        lblname = (TextView) findViewById(R.id.lblname);
        lblorigendestino = (TextView) findViewById(R.id.lblorigendestino);
        txtnumero = (EditText) findViewById(R.id.txtnumero);
        txtavion = (EditText) findViewById(R.id.txtavion);
        txtdate = (EditText) findViewById(R.id.txtdate);
        txtname = (EditText) findViewById(R.id.txtname);
        txtorigendestino = (EditText) findViewById(R.id.txtorigendestino);
        btningresar = (Button) findViewById(R.id.btningresar);
        btnmodificar = (Button) findViewById(R.id.btnmodificar);
        btnconsultar = (Button) findViewById(R.id.btnconsultar);
        btneliminar = (Button) findViewById(R.id.btneliminar);
        btngeneral = (Button) findViewById(R.id.btngeneral);

        btngeneral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, lista.class);
                startActivity(intent);
            }
        });

        btningresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Ruta ruta = new Ruta(txtnumero.getText().toString(), txtavion.getText().toString(),txtdate.getText().toString(),txtname.getText().toString(),txtorigendestino.getText().toString());
                ruta.save();

                //Mensaje
                Toast.makeText(MainActivity.this, "Registro guardado exitosamente", Toast.LENGTH_SHORT).show();
                //Limpiar los campos de texto
                txtnumero.setText("");
                txtavion.setText("");
                txtdate.setText("");
                txtname.setText("");
                txtorigendestino.setText("");
            }
        });

        btnconsultar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    List<Ruta> resultado = Ruta.find(Ruta.class, "Ruta=?", txtnumero.getText().toString());
                    Ruta datos = resultado.get(0);
                    lblnumero.setText(datos.getNumero());
                    lblavion.setText(datos.getAvion());
                    lbldate.setText(datos.getDate());
                    lblname.setText(datos.getName());
                    lblorigendestino.setText(datos.getOrigendestino());
                }catch (Exception e){
                    Log.v("error", "Se crasheo");
                }

            }
        });


        btnmodificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Ruta modificar = Ruta.findById(Ruta.class, Integer.parseInt(txtnumero.getText().toString()));
                modificar.numero = txtavion.getText().toString();
                modificar.save();

                //Mensaje
                Toast.makeText(MainActivity.this, "Registro actualizado exitosamente", Toast.LENGTH_SHORT).show();
                //Limpiar los campos de texto
                txtnumero.setText("");
                txtavion.setText("");
                txtdate.setText("");
                txtname.setText("");
                txtorigendestino.setText("");
            }
        });

        btneliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Ruta eliminarRuta = Ruta.findById(Ruta.class, Integer.parseInt(txtruta.getText().toString()));
                eliminarRuta.delete();*/

                Ruta eliminarRuta = Ruta.findById(Ruta.class,Integer.parseInt(txtnumero.getText().toString()));
                //Ruta eliminarRuta = Ruta.findById(Ruta.class, Integer.parseInt(txtnumero.getText().toString()));

                Ruta.deleteAll(Ruta.class);

                //Mensaje
                Toast.makeText(MainActivity.this, "Registro eliminado exitosamente", Toast.LENGTH_SHORT).show();
                //Limpiar los campos de texto
                txtnumero.setText("");
            }
        });
    }
}
