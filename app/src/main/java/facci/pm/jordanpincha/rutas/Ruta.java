package facci.pm.jordanpincha.rutas;

import com.orm.SugarRecord;

public class Ruta extends SugarRecord {

    String numero, avion, date, name, origendestino;

    public Ruta() {
    }

    public Ruta(String numero, String avion, String date, String name, String origendestino) {
        this.numero = numero;
        this.avion = avion;
        this.date = date;
        this.name = name;
        this.origendestino = origendestino;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getAvion() {
        return avion;
    }

    public void setAvion(String avion) {
        this.avion = avion;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrigendestino() {
        return origendestino;
    }

    public void setOrigendestino(String origendestino) {
        this.origendestino = origendestino;
    }

   /*public Ruta(String numero, String o, String destino, String compañia, String recorrido) {
        this.ruta = ruta;
        this.origen = origen;
        this.destino = destino;
        this.compañia = compañia;
        this.recorrido = recorrido;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getCompañia() {
        return compañia;
    }

    public void setCompañia(String compañia) {
        this.compañia = compañia;
    }

    public String getRecorrido() {
        return recorrido;
    }

    public void setRecorrido(String recorrido) {
        this.recorrido = recorrido;
    }*/
}
